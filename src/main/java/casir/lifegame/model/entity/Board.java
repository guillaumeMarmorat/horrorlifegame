package casir.lifegame.model.entity;

import casir.lifegame.LifeGame;

public class Board
{

    public boolean[][] cells;

    public Board(int boardSize)
    {
        this.cells = new boolean[boardSize][boardSize];
        boolean[] cellInitializationArray;

        for (int lineIndex = 0; lineIndex < boardSize; lineIndex++)
        {
            cellInitializationArray = new boolean[boardSize];
            for (int cellIndex = 0; cellIndex < boardSize; cellIndex++)
            {
                cellInitializationArray[cellIndex] = LifeGame.DEAD;
            }
            this.cells[lineIndex] = cellInitializationArray;
        }
    }

    public boolean isEqual(Board board)
    {
        for (int lineIndex = 0; lineIndex < this.cells.length; lineIndex++)
        {
            for (int cellIndex = 0; cellIndex < this.cells.length; cellIndex++)
            {
                if (this.cells[lineIndex][cellIndex] != board.cells[lineIndex][cellIndex])
                {
                    return false;
                }
            }
        }

        return true;
    }

    public Board clone()
    {
        Board clone = new Board(this.cells.length);
        for (int lineIndex = 0; lineIndex<this.cells.length; lineIndex++)
        {
            for (int cellIndex = 0; cellIndex<this.cells.length; cellIndex++)
            {
                clone.cells[lineIndex][cellIndex] = this.cells[lineIndex][cellIndex];
            }
        }

        return clone;
    }
}