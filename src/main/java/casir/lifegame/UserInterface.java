package casir.lifegame;

import casir.lifegame.model.entity.Board;

public class UserInterface
{

    private static final String CLEAR_CONSOLE = "\033[2J";
    private static final String MOVE_CARET_UP_LEFT = "\033[H";

    private static final String ALIVE_CELL = "#";
    private static final String NO_CELL = "_";

    public static void displayGameBoard(Board board)
    {
        System.out.print(UserInterface.MOVE_CARET_UP_LEFT + UserInterface.CLEAR_CONSOLE);
        System.out.flush();
        String toDisplay;
        for (int lineIndex = 0; lineIndex < 20; lineIndex++)
        {
            toDisplay = "";
            for (int cellIndex = 0; cellIndex < 20; cellIndex++)
            {
                if (board.cells[lineIndex][cellIndex])
                {
                    toDisplay += UserInterface.ALIVE_CELL;
                }
                else
                {
                    toDisplay += UserInterface.NO_CELL;
                }
            }

            System.out.println(toDisplay);
        }
    }

    public static void displayEndMessage()
    {
        System.out.println("Game over");
    }

}