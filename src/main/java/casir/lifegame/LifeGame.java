package casir.lifegame;

import casir.lifegame.model.entity.Board;

public class LifeGame
{
    public static final int DEFAULTSIZE = 20;
    private Board currentBoard;
    private Board previousTurnBoard;
    public static final boolean ALIVE = true;
    public static final boolean DEAD = false;

    public static void main(String[] args)
    {
        LifeGame game = new LifeGame();
        game.play();
        casir.lifegame.UserInterface.displayEndMessage();
    }

    public LifeGame()
    {
        Board defaultBoard = new Board(DEFAULTSIZE);
        Board playingBoard = new Board(DEFAULTSIZE);
        playingBoard.cells[17][17] = ALIVE;
        playingBoard.cells[18][17] = ALIVE;
        playingBoard.cells[17][18] = ALIVE;
        playingBoard.cells[19][18] = ALIVE;
        playingBoard.cells[17][19] = ALIVE;

        this.currentBoard = playingBoard;
        this.previousTurnBoard = defaultBoard;
    }

    private void play()
    {
        while(!isGameOver())
        {
            UserInterface.displayGameBoard(this.currentBoard);

            waitNextTurn();

            playTurn();
        }

        UserInterface.displayEndMessage();
    }

    private boolean isGameOver()
    {
        return this.currentBoard.isEqual(this.previousTurnBoard);
    }

    private void playTurn()
    {
        this.previousTurnBoard =  this.currentBoard.clone();
        this.currentBoard = new Board(DEFAULTSIZE);
        for (int lineIndex = 0; lineIndex < DEFAULTSIZE; lineIndex++)
        {
            for (int cellIndex = 0; cellIndex<DEFAULTSIZE; cellIndex++)
            {
                int neighbourNumber = 0;
                for (int verticalNeighbourIndex = -1; verticalNeighbourIndex <= 1; verticalNeighbourIndex++)
                {
                    for (int horizontalNeighbourIndex = -1; horizontalNeighbourIndex <= 1; horizontalNeighbourIndex++)
                    {
                        if (!(verticalNeighbourIndex== 0 && horizontalNeighbourIndex==0))
                        {
                            if (lineIndex + horizontalNeighbourIndex >= 0
                                    && lineIndex + horizontalNeighbourIndex < DEFAULTSIZE
                                    && cellIndex + verticalNeighbourIndex >= 0
                                    && cellIndex + verticalNeighbourIndex < DEFAULTSIZE)
                            {

                                boolean cellStatus =  this.previousTurnBoard.cells[lineIndex + horizontalNeighbourIndex][cellIndex + verticalNeighbourIndex];
                                if (cellStatus == ALIVE)
                                {
                                    neighbourNumber ++;
                                }


                            }
                        }
                    }
                }
                if ( this.previousTurnBoard.cells[lineIndex][cellIndex])
                {
                    if (neighbourNumber < 2 || neighbourNumber > 3)
                    {
                         this.currentBoard.cells[lineIndex][cellIndex] = DEAD;
                    }
                    else
                    {
                        this.currentBoard.cells[lineIndex][cellIndex] = ALIVE;
                    }
                }
                else
                {
                    if (neighbourNumber == 3)
                    {
                        this.currentBoard.cells[lineIndex][cellIndex] = ALIVE;
                    }
                    else
                    {
                        this.currentBoard.cells[lineIndex][cellIndex] = DEAD;

                    }
                }
            }
        }
    }


    private void waitNextTurn()
    {
        try
        {
            Thread.sleep(100);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}